﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace paintProgram
{   /// <summary>
    /// Form1 class builds gui and controls the events
    /// </summary>
    public partial class Form1 : Form
    {
        // Bitmap used for the output canvass
        //501 pixel size so user can draw to 500
        static int bitmapSize = 501;
        Bitmap OutputBitmap = new Bitmap(bitmapSize, bitmapSize);
        Canvass MyCanvass;
        CommandReader Reader;
        lineReader LReader;

        public Form1()
        {
            InitializeComponent();
            MyCanvass = new Canvass(Graphics.FromImage(OutputBitmap));
            Reader = new CommandReader();
            LReader = new lineReader();
        }
        /// <summary>
        /// commandLine_KeyDown is triggered when a button is pressed in the command prompt text box
        /// </summary>
        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {

            //to get full command, the input is taken after enter is pressed
            if (e.KeyCode == Keys.Enter)
            {
                //trim any extra whitespace 
                string line = commandLine.Text.Trim().ToLower();

                //if run then get text from programInput 
                if (line.Equals("run"))
                {
                    string program = programInput.Text.Trim().ToLower(); //trim programInput whitespace
                    OutputBitmap = Reader.ReadProgram(program);
                    Refresh();
                }
                else  // else its a single command so one line into Reader
                {

                    OutputBitmap = LReader.ReadLine(line, 0);
                    commandLine.Text = "";
                   // errorTextbox.Text(Exception);
                    Refresh();

                }

            }
        }

        private void outputCanvass_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;                    // gets graphics context of pictureBox
            g.DrawImageUnscaled(OutputBitmap, 0, 0);    // paste the bitmap onto pictureBox

        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //clear the canvass when new button in menu pressed
            Graphics g = Graphics.FromImage(OutputBitmap); 
            g.Clear(Color.White);
            Refresh();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //save image to chosen file path
            SaveFileDialog dialog = new SaveFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                OutputBitmap.Save(dialog.FileName);
            }
        }
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //exit when menu>close pressed
            Application.Exit();
        }

        private void aboutInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //pop up window showing info of program 
            System.Windows.Forms.MessageBox.Show("This program was designed by Lochlainn Marynicz. " +
                "\n\nTo view the source code and download the latest version visit https://bitbucket.org/turtleloch/c3548230-project-rep/src/master/", "About - Paint Program");

        }

        private void guideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //pop up showing possible commands guide
            System.Windows.Forms.MessageBox.Show("x,y,t,d,s represent integers relating to the position or size within the drawing \n\n" +
                                                 "Commands: drawto x,y  -  Draws a line from current point to x,y point \n" +
                                                 "                     moveto x,y  -  Moves the point without drawing \n" +
                                                 "                     thickness t -  Changes the thickness of the pen \n" +
                                                 "                     circle d       -  Draws a circle of diameter d \n" +
                                                 "                     square s     -  Draws a square with side length s \n" +
                                                 "                     triagnle s   -  Draws a triangle with side length s \n" +
                                                 "                     fill on/off   -  Changes whether the shape drawn is filled", "Command Guide");


        }
        /// <summary>
        /// method runs any of chosen keywords through checkKeyword method
        /// </summary>
        private void programInput_TextChanged(object sender, EventArgs e)
        {
            CheckKeyword("pen", Color.Blue, 0); //add all chosen keywords
            CheckKeyword("fill", Color.Blue, 0);
            CheckKeyword("moveto", Color.Blue, 0);
            CheckKeyword("drawto", Color.Blue, 0);
            CheckKeyword("square", Color.Blue, 0);
            CheckKeyword("circle", Color.Blue, 0);
            CheckKeyword("rect", Color.Blue, 0);
            CheckKeyword("triangle", Color.Blue, 0);
        }
        /// <summary>
        /// checks for word in programInput text box and changes colour of text to blue if its a keyword
        /// </summary>
        /// <param name="word">words passed in to check </param>
        /// <param name="color">colour passed in (blue) </param>
        /// <param name="startIndex"> start point to check words from </param>
        private void CheckKeyword(string word, Color color, int startIndex)
        {
            int index = -1;
            int selectStart = this.programInput.SelectionStart;

            if (programInput.Text.Contains(word))
            {
 

                while ((index = this.programInput.Text.IndexOf(word, (index + 1))) != -1)
                {
                    programInput.Select((index + startIndex), word.Length);
                    programInput.SelectionColor = color;
                    programInput.Select(selectStart, 0);
                    programInput.SelectionColor = Color.Black;
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
        private void ProgramAreaLabel_Click(object sender, EventArgs e)
        {

        }
        private void outputWindow_TextChanged(object sender, EventArgs e)
        {

        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            if (open.ShowDialog() == DialogResult.OK)
                outputCanvass.Image = Bitmap.FromFile(open.FileName);

        }

        private void errorTextbox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

