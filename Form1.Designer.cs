﻿namespace paintProgram
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.commandLine = new System.Windows.Forms.TextBox();
            this.outputCanvass = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.XAxisLabel = new System.Windows.Forms.Label();
            this.YAxisLabel = new System.Windows.Forms.Label();
            this.ProgramAreaLabel = new System.Windows.Forms.Label();
            this.CommandLineAreaLabel = new System.Windows.Forms.Label();
            this.programInput = new System.Windows.Forms.RichTextBox();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.outputCanvass)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // commandLine
            // 
            this.commandLine.Location = new System.Drawing.Point(24, 77);
            this.commandLine.Margin = new System.Windows.Forms.Padding(2);
            this.commandLine.Name = "commandLine";
            this.commandLine.Size = new System.Drawing.Size(354, 20);
            this.commandLine.TabIndex = 1;
            this.commandLine.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.commandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.commandLine_KeyDown);
            // 
            // outputCanvass
            // 
            this.outputCanvass.BackColor = System.Drawing.Color.White;
            this.outputCanvass.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.outputCanvass.Location = new System.Drawing.Point(430, 58);
            this.outputCanvass.Margin = new System.Windows.Forms.Padding(2);
            this.outputCanvass.Name = "outputCanvass";
            this.outputCanvass.Size = new System.Drawing.Size(505, 505);
            this.outputCanvass.TabIndex = 3;
            this.outputCanvass.TabStop = false;
            this.outputCanvass.Click += new System.EventHandler(this.pictureBox1_Click);
            this.outputCanvass.Paint += new System.Windows.Forms.PaintEventHandler(this.outputCanvass_Paint);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(953, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutInfoToolStripMenuItem,
            this.guideToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutInfoToolStripMenuItem
            // 
            this.aboutInfoToolStripMenuItem.Name = "aboutInfoToolStripMenuItem";
            this.aboutInfoToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.aboutInfoToolStripMenuItem.Text = "About info";
            this.aboutInfoToolStripMenuItem.Click += new System.EventHandler(this.aboutInfoToolStripMenuItem_Click);
            // 
            // guideToolStripMenuItem
            // 
            this.guideToolStripMenuItem.Name = "guideToolStripMenuItem";
            this.guideToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.guideToolStripMenuItem.Text = "Guide";
            this.guideToolStripMenuItem.Click += new System.EventHandler(this.guideToolStripMenuItem_Click);
            // 
            // XAxisLabel
            // 
            this.XAxisLabel.AutoSize = true;
            this.XAxisLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XAxisLabel.Location = new System.Drawing.Point(427, 43);
            this.XAxisLabel.Name = "XAxisLabel";
            this.XAxisLabel.Size = new System.Drawing.Size(504, 13);
            this.XAxisLabel.TabIndex = 9;
            this.XAxisLabel.Text = "X 0                                                        250                   " +
    "                                  500";
            // 
            // YAxisLabel
            // 
            this.YAxisLabel.AutoSize = true;
            this.YAxisLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YAxisLabel.Location = new System.Drawing.Point(400, 61);
            this.YAxisLabel.Name = "YAxisLabel";
            this.YAxisLabel.Size = new System.Drawing.Size(28, 494);
            this.YAxisLabel.TabIndex = 10;
            this.YAxisLabel.Text = "Y 0\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n250\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n5" +
    "00";
            // 
            // ProgramAreaLabel
            // 
            this.ProgramAreaLabel.AutoSize = true;
            this.ProgramAreaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProgramAreaLabel.Location = new System.Drawing.Point(21, 127);
            this.ProgramAreaLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ProgramAreaLabel.Name = "ProgramAreaLabel";
            this.ProgramAreaLabel.Size = new System.Drawing.Size(212, 17);
            this.ProgramAreaLabel.TabIndex = 11;
            this.ProgramAreaLabel.Text = "Write or enter program here";
            this.ProgramAreaLabel.Click += new System.EventHandler(this.ProgramAreaLabel_Click);
            // 
            // CommandLineAreaLabel
            // 
            this.CommandLineAreaLabel.AutoSize = true;
            this.CommandLineAreaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CommandLineAreaLabel.Location = new System.Drawing.Point(21, 58);
            this.CommandLineAreaLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CommandLineAreaLabel.Name = "CommandLineAreaLabel";
            this.CommandLineAreaLabel.Size = new System.Drawing.Size(220, 17);
            this.CommandLineAreaLabel.TabIndex = 12;
            this.CommandLineAreaLabel.Text = "Enter Single Commands Here";
            // 
            // programInput
            // 
            this.programInput.Location = new System.Drawing.Point(24, 160);
            this.programInput.Name = "programInput";
            this.programInput.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.programInput.Size = new System.Drawing.Size(354, 403);
            this.programInput.TabIndex = 13;
            this.programInput.Text = "";
            this.programInput.TextChanged += new System.EventHandler(this.programInput_TextChanged);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(953, 582);
            this.Controls.Add(this.programInput);
            this.Controls.Add(this.CommandLineAreaLabel);
            this.Controls.Add(this.ProgramAreaLabel);
            this.Controls.Add(this.YAxisLabel);
            this.Controls.Add(this.XAxisLabel);
            this.Controls.Add(this.outputCanvass);
            this.Controls.Add(this.commandLine);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Paint Program - Lochlainn Marynicz";
            ((System.ComponentModel.ISupportInitialize)(this.outputCanvass)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox commandLine;
        private System.Windows.Forms.PictureBox outputCanvass;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guideToolStripMenuItem;
        private System.Windows.Forms.Label XAxisLabel;
        private System.Windows.Forms.Label YAxisLabel;
        private System.Windows.Forms.Label ProgramAreaLabel;
        private System.Windows.Forms.Label CommandLineAreaLabel;
        private System.Windows.Forms.RichTextBox programInput;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
    }
}

