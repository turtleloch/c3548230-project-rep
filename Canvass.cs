﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace paintProgram
{
    public class Canvass
    {
        /// <summary>
        /// Canvass Class holds all the drawing methods called on by commandReader to draw shapes
        /// </summary>


        // create variables needed
        Graphics g;
        Pen Pen;
        Pen reticlePen; //pen for reticle needs its own unchanging colour   
        Pen clearPen;   //this pen used 
        SolidBrush Brush;
        SolidBrush clearBrush;
        int xPos, yPos, penThickness;
        Color penColour = Color.Black;
        int reticleWidth = 1;
        Boolean fill;


        public Canvass(Graphics g)
        {
            //set up constants & variables
            this.g = g;
            xPos = yPos = 0;
            penThickness = 1;
            Pen = new Pen(penColour, penThickness);
            Brush = new SolidBrush(penColour);
            clearPen = new Pen(Color.White, reticleWidth);
            reticlePen = new Pen(Color.Red, reticleWidth);
            clearBrush = new SolidBrush(Color.White);
            fill = false;

            //draw initial reticle point 
            g.DrawRectangle(reticlePen, xPos, yPos, reticleWidth, reticleWidth);

        }
        /// <summary>
        /// Method to move the pen to specified position
        /// </summary>
        /// <param name="toX"> new x position </param>
        /// <param name="toY"> new y possition </param>
        public void MovePen(int toX,int toY)
        {
            g.DrawRectangle(clearPen, xPos, yPos, reticleWidth,reticleWidth);
            xPos = toX;
            yPos = toY;
            g.DrawRectangle(reticlePen, xPos, yPos, reticleWidth, reticleWidth);
        }
        /// <summary>
        /// methods to change pen colour
        /// </summary>
        public void PenRed()
        {
            penColour = Color.Red;
            Pen = new Pen(penColour, penThickness);
            Brush = new SolidBrush(penColour);


        }
        public void PenBlue()
        {
            penColour = Color.Blue;
            Pen = new Pen(penColour, penThickness);
            Brush = new SolidBrush(penColour);

        }
        public void PenGreen()
        {
            penColour = Color.Green;
            Pen = new Pen(penColour, penThickness);
            Brush = new SolidBrush(penColour);

        }
        public void PenBlack()
        {
            penColour = Color.Black;
            Pen = new Pen(penColour, penThickness);
            Brush = new SolidBrush(penColour);

        }
        public void PenOrange()
        {
            penColour = Color.Orange;
            Pen = new Pen(penColour, penThickness);
            Brush = new SolidBrush(penColour);

        }
        /// <summary>
        /// method to draw single line to new position
        /// </summary>
        /// <param name="toX"> destination x position </param>
        /// <param name="toY"> destination y position </param>
        public void DrawLine(int toX, int toY)

        {
            //Draw a line to new x and y position,
            //update new x and y positions
            g.DrawRectangle(clearPen, xPos, yPos, reticleWidth, reticleWidth);
            g.DrawLine(Pen, xPos, yPos, toX, toY); 
            xPos = toX;
            yPos = toY;
            g.DrawRectangle(reticlePen, xPos, yPos, reticleWidth, reticleWidth);

        }

        /// <summary>
        /// Method to draw a square
        /// </summary>
        /// <param name="width"> width specified for the square </param>
        public void DrawSquare(int size)
        {
            if (fill == false)
            {
                g.DrawRectangle(Pen, xPos, yPos, size, size);
            }
            else if (fill == true)
            {
                g.FillRectangle(Brush, xPos, yPos, size, size);
            }
            
        }

        /// <summary>
        /// method to draw a circle 
        /// </summary>
        /// <param name="d">diameter of circle to be drawn</param>
        public void DrawCircle(int d)
        {
            //half of d = radius
            int r = d / 2;

            //create rectangle (square) for ellipse (cirlce) to border
            Rectangle rect = new Rectangle(xPos -r , yPos -r, d, d);

            // Draw ellipse to screen.
            if (fill ==false)
            {
                g.DrawEllipse(Pen, rect);
            }
            else if (fill == true)
            {
                g.FillEllipse(Brush, rect);
            }

        }
        /// <summary>
        /// method to draw a rectangle 
        /// </summary>
        /// <param name="width"> width of rectangle </param>
        /// <param name="height"> height of rectangle </param>
        public void DrawRectangle(int width, int height)
        {
            if (fill == false)
            {
                g.DrawRectangle(Pen, xPos, yPos, width, height);
            }
            else if (fill == true)
            {
                g.FillRectangle(Brush, xPos, yPos, width, height);
            }
            
        }
        /// <summary>
        /// method to draw right angle triangle
        /// </summary>
        /// <param name="size"> length of 2 sides of triangle </param>
        public void DrawTriangle(int size)
        {
            Point[] Points = { new Point(xPos,yPos),
                                   new Point(xPos+size,yPos+size),
                                   new Point(xPos+size,yPos),
                                 };
            if (fill == false)
            {
                g.DrawPolygon(Pen, Points);

            }
            else if (fill == true)
            {

                g.FillPolygon(Brush, Points);
              
            }
        }
        /// <summary>
        /// method to change thickness of pen
        /// </summary>
        /// <param name="newThickness"></param>
        public void ChangePenThickness(int newThickness)
        {
            penThickness = newThickness;
            Pen = new Pen(penColour, penThickness);
        }
        /// <summary>
        /// method to change whether shape is filled in colour
        /// </summary>
        /// <param name="e"> boolean true if filled, false if not </param>
        public void ChangeFill(Boolean e)
        {
            fill = e;
        }
        /// <summary>
        /// method to clear the canvass, leaves x,y position to last 
        /// </summary>
        public void clearCanvass()
        {
            g.Clear(Color.Transparent);
            g.DrawRectangle(reticlePen, xPos, yPos, reticleWidth, reticleWidth);
        }
        /// <summary>
        /// method to reset the canvass and reset x,y position to 0,0
        /// </summary>
        public void resetCanvass()
        {
            clearCanvass();
            g.DrawRectangle(clearPen, xPos, yPos, reticleWidth, reticleWidth);
            xPos = 0;
            yPos = 0;
            g.DrawRectangle(reticlePen, xPos, yPos, reticleWidth, reticleWidth);


        }

    }
}