﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Linq.Expressions;

namespace paintProgram
{
    public class lineReader
    {
        /// <summary>
        /// set up new bitmap to then update to outputBitmap
        /// </summary>
        Canvass MyCanvass;
        static int bitmapSize = 501; // 501 size bitmap, so user can draw upto x/y=500
        Bitmap Bitmap = new Bitmap(bitmapSize, bitmapSize);
        Var var;

        public lineReader()
        {
            MyCanvass = new Canvass(Graphics.FromImage(Bitmap));


        }
        /// <summary>
        /// method to parse a command line / program input line
        /// </summary>
        /// <param name="line"> selected line to parse </param>
        /// <param name="lineNumber"> keeps count of which line in program is being read to tell user if error </param>
        /// <returns> return bitmap to refresh output bitmap </returns>
        public Bitmap ReadLine(string line, int lineNumber)
        {
            int varx, vary;
            //take the input into a string and split on a space to get parts of the command line

            string[] splitLine = line.Split(' '); //for drawing commands, split the coordanates from the command and then split by comma to get each number
            string Command = splitLine[0];

            if (splitLine.Length > 1)      //select whether to split line again if more than one word in command line
            {

                string[] Coords = splitLine[1].Split(',');  //split command coordinates 
                int[] intCoords = new int[Coords.Length];
               

                if (Coords.Length >= 2) //if second part of split is > 2, it must be a shape command with numbers
                {
                    for (int i = 0; i < Coords.Length; i++) //convert string numbers into ints, catch if error
                    {

                        try
                        {
                            intCoords[i] = int.Parse(Coords[i]);
                        }
                        catch (Exception)
                        {
                            System.Windows.Forms.MessageBox.Show("Parameters cannot be converted to int.\n Error on line: " + lineNumber, "Error");


                        }
                    }
                }
                if (Command.Equals("drawto"))
                {
                    try
                    {
                        MyCanvass.DrawLine(intCoords[0], intCoords[1]);
                        return Bitmap;
                    }
                    catch (System.IndexOutOfRangeException)
                    {

                        System.Windows.Forms.MessageBox.Show("Out of range error for parameter on line: " + lineNumber, "Error");
                    }

                }
                else if (Command.Equals("moveto"))
                {
                    try
                    {
                        MyCanvass.MovePen(intCoords[0], intCoords[1]);
                        return Bitmap;
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        System.Windows.Forms.MessageBox.Show("Out of range error for parameter on line: " + lineNumber, "Error");

                    }
                    catch (System.FormatException)
                    {
                        System.Windows.Forms.MessageBox.Show("Format error for parameter on line: " + lineNumber, "Error");
                    }

                }

                else if (Command.Equals("x"))
                {
                    Console.WriteLine(splitLine[2]);
                    varx = int.Parse(splitLine[2]);

                }
                else if (Command.Equals("y"))
                {

                    var.sety(int.Parse(splitLine[2]));

                }
                else if (Command.Equals("circle"))
                {
                    try
                    {
                        
                        Console.WriteLine(varx);
                        MyCanvass.DrawCircle(varx);//for cicle only 1 number is passed in so use X
                        return Bitmap;
                    }
                    catch (System.FormatException)
                    {
                        System.Windows.Forms.MessageBox.Show("Format error for parameter on line: " + lineNumber, "Error");
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        System.Windows.Forms.MessageBox.Show("Out of range error for parameter on line: " + lineNumber, "Error");
                    }
                }

                else if (Command.Equals("square"))
                {
                    try
                    {
                        MyCanvass.DrawSquare(int.Parse(Coords[0])); //similarly just 1 number passed in so using X
                        return Bitmap;
                    }
                    catch (System.FormatException)
                    {
                        System.Windows.Forms.MessageBox.Show("Format error for parameter on line: " + lineNumber, "Error");
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        System.Windows.Forms.MessageBox.Show("Out of range error for parameter on line: " + lineNumber, "Error");
                    }

                }

                else if (Command.Equals("rect"))
                {

                    try
                    {
                        MyCanvass.DrawRectangle(intCoords[0], intCoords[1]);
                        return Bitmap;
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        System.Windows.Forms.MessageBox.Show("Out of range error for parameters on line: " + lineNumber, "Error");
                    }
                    catch (System.FormatException)
                    {
                        System.Windows.Forms.MessageBox.Show("Format error for parameters on line: " + lineNumber, "Error");
                    }
                }
                else if (Command.Equals("triangle"))
                {
                    try
                    {
                        MyCanvass.DrawTriangle(int.Parse(Coords[0]));
                        return Bitmap;
                    }
                    catch (System.FormatException)
                    {
                        System.Windows.Forms.MessageBox.Show("Format error for parameters on line: " + lineNumber, "Error");
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        System.Windows.Forms.MessageBox.Show("Out of range error for parameters on line: " + lineNumber, "Error");
                    }
                }
                else if (Command.Equals("thickness"))
                {

                    try
                    {
                        int intThickness = int.Parse(Coords[0]);
                        if (intThickness <= 0)
                        {
                            System.Windows.Forms.MessageBox.Show("Error on line: " + lineNumber + ". Thickness must be higher than 0", "Error");
                        }
                        else
                        {
                            MyCanvass.ChangePenThickness(intThickness);
                            return Bitmap;
                        }
                    }
                    catch (System.FormatException)
                    {
                        System.Windows.Forms.MessageBox.Show("Format error for parameter  on line: " + lineNumber, "Error");
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        System.Windows.Forms.MessageBox.Show("Out of range error for parameter  on line: " + lineNumber, "Error");
                    }
                }
                else if (Command.Equals("fill"))
                {
                    try
                    {
                        if (splitLine[1].Equals("on"))
                        {
                            MyCanvass.ChangeFill(true);
                        }
                        if (splitLine[1].Equals("off"))
                        {
                            MyCanvass.ChangeFill(false);
                        }
                    }
                    catch (System.FormatException)
                    {
                        System.Windows.Forms.MessageBox.Show("Format error on line: " + lineNumber, "Error");
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        System.Windows.Forms.MessageBox.Show("Out of range error on line: " + lineNumber, "Error");
                    }


                }
                else if (Command.Equals("pen"))
                {
                    try
                    {
                        if (splitLine[1].Equals("red"))
                        {
                            MyCanvass.PenRed();
                            return Bitmap;
                        }
                        else if (splitLine[1].Equals("black"))
                        {
                            MyCanvass.PenBlack();
                            return Bitmap;
                        }
                        else if (splitLine[1].Equals("blue"))
                        {
                            MyCanvass.PenBlue();
                            return Bitmap;
                        }
                        else if (splitLine[1].Equals("green"))
                        {
                            MyCanvass.PenGreen();
                            return Bitmap;
                        }
                        else if (splitLine[1].Equals("orange"))
                        {
                            MyCanvass.PenOrange();
                            return Bitmap;
                        }

                    }
                    catch (System.FormatException)
                    {
                        System.Windows.Forms.MessageBox.Show("Format error for colour name on line: " + lineNumber, "Error");
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        System.Windows.Forms.MessageBox.Show("Out of range error for colour name on line: " + lineNumber, "Error");
                    }
                }
            }
            
            else if (Command.Equals("save"))
            {
                try
                {
                    SaveFileDialog dialog = new SaveFileDialog();
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        Bitmap.Save(dialog.FileName);
                    }
                }
                catch (System.FormatException)
                {
                    System.Windows.Forms.MessageBox.Show("Format error on line: " + lineNumber, "Error");
                }
                catch (System.IndexOutOfRangeException)
                {
                    System.Windows.Forms.MessageBox.Show("Out of range error on line: " + lineNumber, "Error");
                }

            }
            else if (Command.Equals("new"))
            {
                try
                {
                    MyCanvass.clearCanvass();
                    return Bitmap;
                }
                catch (System.FormatException)
                {
                    System.Windows.Forms.MessageBox.Show("Format error on line: " + lineNumber, "Error");
                }
                catch (System.IndexOutOfRangeException)
                {
                    System.Windows.Forms.MessageBox.Show("Out of range error on line: " + lineNumber, "Error");
                }


            }
            else if (Command.Equals("reset"))
            {
                try
                {
                    MyCanvass.resetCanvass();
                    return Bitmap;
                }
                catch (System.FormatException)
                {
                    System.Windows.Forms.MessageBox.Show("Format error on line: " + lineNumber, "Error");
                }
                catch (System.IndexOutOfRangeException)
                {
                    System.Windows.Forms.MessageBox.Show("Out of range error on line: " + lineNumber, "Error");
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Error with command on line: " + lineNumber, "Error");
                return Bitmap;
            }
            return Bitmap;
        }
    }
}
